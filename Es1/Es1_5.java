import java.lang.Character;

public class Es1_5 {
    public static boolean scan(String s) {
	int state = 0;
	int i = 0;

	while (state >= 0 && i < s.length()) {
	    final char ch = s.charAt(i++);

	    switch (state) {
	    case 0:
	    	if (ch >= 'A' && ch <= 'K')
			state = 1;
		else if (ch >= 'L' && ch <= 'Z')
			state = 2;
		else
			state = -1;
		break;

	    case 1:
	    	if (ch >= 'a' && ch <= 'z')
			state = 1;
		else if (isEven(ch))
			state = 3;
		else if (isOdd(ch))
			state = 4;
		else
			state = -1;
                break;

	    case 2:
	    	if (ch >= 'a' && ch <= 'z')
			state = 2;
		else if (isEven(ch))
			state = 5;
		else if (isOdd(ch))
			state = 6;
		else
			state = -1;
		break;

	    case 3:
	    	if (isEven(ch))
			state = 3;
		else if (isOdd(ch))
			state = 4;
		else
			state = -1;
		break;

	    case 4:
	    	if (isEven(ch))
			state = 3;
		else if (isOdd(ch))
			state = 4;
		else
			state = -1;
		break;
	
	    case 5:
	    	if (isEven(ch))
			state = 5;
		else if (isOdd(ch))
			state = 6;
		else
			state = -1;
		break;
	    	
	    case 6:
	    	if (isEven(ch))
			state = 5;
		else if (isOdd(ch))
			state = 6;
		else
			state = -1;
		break;
	    }
	}

	return ( state == 3 || state == 6 );
    }

    public static void main(String[] args) {
	System.out.println(scan(args[0]) ? "OK" : "NOPE");
    }

    private static boolean isEven(char ch) {
        return Character.isDigit(ch) && (Character.getNumericValue(ch) % 2 == 0);
    }

    private static boolean isOdd(char ch) {
        return Character.isDigit(ch) && (Character.getNumericValue(ch) % 2 == 1);
    }
}
