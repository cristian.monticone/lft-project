public class Es1_9 {
    public static boolean scan(String s) {
	int state = 0;
	int i = 0;

	while (state >= 0 && i < s.length()) {
	    final char ch = s.charAt(i++);

	    switch (state) {
	    case 0:
	    	if (ch == 'c')
			state = 1;
		else // Symbols complementari a ch, considerando l'insieme dei
		     // caratteri UNICODE(il tipo primitivo char di java
		     // rappresenta caratteri Unicode) come alfabeto Sigma.
			state = 9;
		break;

	    case 1:
	    	if (ch == 'r')
			state = 2;
		else
			state = 10;
		break;

	    case 2:
	    	if (ch == 'i')
			state = 3;
		else
			state = 11;
		break;

	    case 3:
	    	if (ch == 's')
			state = 4;
		else
			state = 12;
		break;

	    case 4:
	    	if (ch == 't')
			state = 5;
		else
			state = 13;
		break;

	    case 5:
	    	if (ch == 'i')
			state = 6;
		else
			state = 14;
		break;

	    case 6:
	    	if (ch == 'a')
			state = 7;
		else
			state = 15;
		break;

	    case 7:
	    	state = 8;
		break;

	    case 8:
	    	state = -1;
		break;

	    case 9:
	    	if (ch == 'r')
			state = 10;
		else
			state = -1;
		break;

	    case 10:
	    	if (ch == 'i')
			state = 11;
		else
			state = -1;
		break;

	    case 11:
	    	if (ch == 's')
			state = 12;
		else
			state = -1;
		break;

	    case 12:
	    	if (ch == 't')
			state = 13;
		else
			state = -1;
		break;

	    case 13:
	    	if (ch == 'i')
			state = 14;
		else
			state = -1;
		break;

	    case 14:
	    	if (ch == 'a')
			state = 15;
		else
			state = -1;
		break;

	    case 15:
	    	if (ch == 'n')
			state = 8;
		else
			state = -1;
		break;
	    }
	}

	return state == 8;
    }

    public static void main(String[] args) {
	System.out.println(scan(args[0]) ? "OK" : "NOPE");
    }
}
