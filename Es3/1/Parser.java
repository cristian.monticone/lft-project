import java.io.*;

public class Parser {
    private Lexer lex;
    private BufferedReader pbr;
    private Token look;

    public Parser(Lexer l, BufferedReader br) {
        lex = l;
        pbr = br;
        move();
    }

    void move() {
        look = lex.lexical_scan(pbr);
        System.out.println("token = " + look);
    }

    void error(String s) {
	throw new Error("near line " + lex.line + ": " + s);
    }

    void match(int t) {
	if (look.tag == t) {
	    if (look.tag != Tag.EOF) move();
	} else error("syntax error");
    }

    private boolean testGui(int... tags) {
	for (int tag : tags)
		if (look.tag == tag) return true;

	return false;
    }

    public void start() {
    	if (testGui('(', Tag.NUM)) {
		expr();
		match(Tag.EOF);
	}
	else {
		error("An expressione must start with '(' or a numeric value.");
	}
    }

    private void expr() {
    	if (testGui('(', Tag.NUM)) {
		term();
		exprp();
	}
	else {
		error("Expected an expression witch start with '(' or with a numeric value.");
	}
    }

    private void exprp() {
	if (testGui('+')) {
		match('+');
		term();
		exprp();
	}
	else if (testGui('-')) {
		match('-');
		term();
		exprp();
	}
	else if (testGui(')', Tag.EOF)) {
		// zzZ....
	}
	else {
		error("An expression must finish with ')' or with the end of file.");
	}
    }

    private void term() {
    	if (testGui('(', Tag.NUM)) {
		fact();
		termp();
	}
	else {
		error("A term must start with a factor!");
	}
    }

    private void termp() {
    	if (testGui('*')) {
		match('*');
		fact();
		termp();
	}
	else if (testGui('/')) {
		match('/');
		fact();
		termp();
	}
	else if (testGui(Tag.EOF, '+', '-', ')')) {
		// do nothing! zzZ....
	}
	else {
		error("The expression must continue(+,-,/,*) or end(')',EOF), unexpected token.");
	}
    }

    private void fact() {
    	if (testGui('(')) {
		match('(');
		expr();
		match(')');
	}
	else if (testGui(Tag.NUM)) {
		match(Tag.NUM);
	}
	else {
		error("Expected a factor!");
	}
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = args[0]; // il percorso del file da leggere
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Parser parser = new Parser(lex, br);
            parser.start();
            System.out.println("Input OK");
            br.close();
        } catch (IOException e) {e.printStackTrace();}
    }
}
