import java.io.*;
import java.util.*;

public class Parser {
    private Lexer lex;
    private BufferedReader pbr;
    private Token look;

    public Parser(Lexer l, BufferedReader br) {
        lex = l;
        pbr = br;
        move();
    }

    void move() {
        look = lex.lexical_scan(pbr);
        System.out.println("token = " + look);
    }

    void error(String s) {
	throw new Error("near line " + lex.line + ": " + s);
    }

    void match(int t) {
	if (look.tag == t) {
	    if (look.tag != Tag.EOF) move();
	} else error("syntax error");
    }

    private boolean testGui(int... tags) {
	for (int tag : tags)
		if (look.tag == tag) return true;

	return false;
    }

    public void prog() {
    	if (testGui(Tag.ID, Tag.PRINT, Tag.READ, Tag.CASE, Tag.WHILE, '{')) {
		statlist();
		match(Tag.EOF);
	}
	else {
		error("A program must start with a statement!");
	}
    }

    private void statlist() {
    	if (testGui(Tag.ID, Tag.PRINT, Tag.READ, Tag.CASE, Tag.WHILE, '{')) {
		stat();
		statlistp();
	}
	else {
		error("Expected a statement.");
	}
    }

    private void statlistp() {
    	if (testGui(';')) {
		move();
		stat();
		statlistp();
	}
	else if (testGui(Tag.EOF, '}')) {
		// Do nothing! all right :-)
	}
	else {
		error("Unexpected symbol after statement list.");
	}
    }

    private void stat() {
    	if (testGui(Tag.ID)) {
		match(Tag.ID);
		match(Tag.ASSIGN);
		expr();
	}
	else if (testGui(Tag.PRINT)) {
		match(Tag.PRINT);
		match('(');
		expr();
		match(')');
	}
	else if (testGui(Tag.READ)) {
		match(Tag.READ);
		match('(');
		match(Tag.ID);
		match(')');
	}
	else if (testGui(Tag.CASE)) {
		match(Tag.CASE);
		whenlist();
		match(Tag.ELSE);
		stat();
	}
	else if (testGui(Tag.WHILE)) {
		match(Tag.WHILE);
		match('(');
		bexpr();
		match(')');
		stat();
	}
	else if (testGui('{')) {
		match('{');
		statlist();
		match('}');
	}
	else {
		error("Expected a statement! Unexpected token.");
	}
    }

    private void whenlist() {
    	if (testGui(Tag.WHEN)) {
		whenitem();
		whenlistp();
	}
	else {
		error("A 'case when list' must start with a when item.");
	}
    }

    private void whenlistp() {
    	if (testGui(Tag.WHEN)) {
		whenitem();
		whenlistp();
	}
	else if (testGui(Tag.ELSE)) {
		// Nothing to do here
	}
	else {
		error("Expected a WHEN or an ELSE! Syntax error...");
	}
    }

    private void whenitem() {
    	if (testGui(Tag.WHEN)) {
		match(Tag.WHEN);
		match('(');
		bexpr();
		match(')');
		stat();
	}
	else {
		error("Syntax error...");
	}
    }

    private void bexpr() {
    	if (testGui('(', Tag.NUM, Tag.ID)) {
		expr();
		match(Tag.RELOP);
		expr();
	}
	else {
		error("Unexpected start in the when test! Expected the first" +
		      " expression to start with a '(', a number " +
		      "or a identifier!");
	}
    }

    private void expr() {
    	if (testGui('(', Tag.ID, Tag.NUM)) {
		term();
		exprp();
	}
	else {
		error("Expected an expression witch start with a factor: a '(', a " +
		      "identifier or a numeric value.");
	}
    }

    private void exprp() {
	if (testGui('+')) {
		match('+');
		term();
		exprp();
	}
	else if (testGui('-')) {
		match('-');
		term();
		exprp();
	}
	else if (testGui(Tag.EOF, ')', Tag.RELOP, '}', ';', Tag.WHEN, Tag.ELSE, Tag.RELOP)) {
		// nop
	}
	else {
		error("Unexpected token after one expression.");
	}
    }

    private void term() {
    	if (testGui('(', Tag.ID, Tag.NUM)) {
		fact();
		termp();
	}
	else {
		error("A term must start with a factor!");
	}
    }

    private void termp() {
    	if (testGui('*')) {
		match('*');
		fact();
		termp();
	}
	else if (testGui('/')) {
		match('/');
		fact();
		termp();
	}
	else if (testGui('+', '-', '}', ';', ')', Tag.EOF, Tag.WHEN, Tag.ELSE, Tag.RELOP)) {
		// do nothing!
	}
	else {
		error("The expression must continue(+,-,/,*) or end, unexpected token.");
	}
    }

    private void fact() {
    	if (testGui('(')) {
		move();
		expr();
		match(')');
	}
	else if (testGui(Tag.ID, Tag.NUM)) {
		move();
	}
	else {
		error("Expected a factor!");
	}
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = args[0]; // il percorso del file da leggere
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Parser parser = new Parser(lex, br);
            parser.prog();
            System.out.println("Input OK");
            br.close();
        } catch (IOException e) {e.printStackTrace();}
    }
}
