package lexer;

import java.io.*; 
import java.util.*;

public class Lexer {

    public static int line = 1;
    private char peek = ' ';
    
    private void readch(BufferedReader br) {
        try {
            peek = (char) br.read();
        } catch (IOException exc) {
            peek = (char) -1; // ERROR
        }
    }

    private void trim(BufferedReader br) {
        while (peek == ' ' || peek == '\t' || peek == '\n'  || peek == '\r') {
            if (peek == '\n') line++;
            readch(br);
        }
    }

    public Token lexical_scan(BufferedReader br) {
    	while (peek == ' ' || peek == '\t' || peek == '\n'  || peek == '\r' || peek == '/') {
		trim(br);
		
		// Ignome comments.
		if (peek == '/') {
			readch(br);
			if (peek == '*') {
				boolean flag = true;
				while (flag) {
					readch(br);
					if (peek == '*') {
						readch(br);
						if (peek == '/')
							flag = false;
					}
				}
				peek = ' ';
			}
			else if (peek == '/') {
				boolean flag = true;
				while (flag) {
					readch(br);
					if (peek == (char)-1) {
						flag = false;
					}
					else if (peek == '\n') {
						line++;
						flag = false;
					}
				}
				peek = ' ';
			}
			else {
				return Token.div;
			}
		}
	}

        switch (peek) {
            case '!':
                peek = ' ';
                return Token.not;

	    case '(':
	    	peek = ' ';
		return Token.lpt;

	    case ')':
	    	peek = ' ';
		return Token.rpt;

	    case '{':
	    	peek = ' ';
		return Token.lpg;

	    case '}':
	    	peek = ' ';
		return Token.rpg;

	    case '+':
	    	peek = ' ';
		return Token.plus;

	    case '-':
	    	peek = ' ';
		return Token.minus;

	    case '*':
	    	peek = ' ';
		return Token.mult;

	    case ';':
	    	peek = ' ';
		return Token.semicolon;

            case '&':
                readch(br);
                if (peek == '&') {
                    peek = ' ';
                    return Word.and;
                } else {
                    System.err.println("Erroneous character at line " + line
                            + " after & : "  + peek );
                    return null;
                }

            case '|':
                readch(br);
                if (peek == '|') {
                    peek = ' ';
                    return Word.or;
                } else {
                    System.err.println("Erroneous character at line " + line
                            + " after | : "  + peek );
                    return null;
                }

            case '<':
                readch(br);
                if (peek == '=') {
                    peek = ' ';
                    return Word.le;
		}
		else if (peek == '>') {
		    peek = ' ';
		    return Word.ne;
                } else {
		    // Il nuovo peek deve essere valutato nella iterazione successiva.
		    return Word.lt;
                }

            case '>':
                readch(br);
                if (peek == '=') {
                    peek = ' ';
                    return Word.ge;
                } else {
		    return Word.gt;
                }

            case '=':
                readch(br);
                if (peek == '=') {
                    peek = ' ';
                    return Word.eq;
                } else {
                    System.err.println("Erroneous character at line " + line
                            + " after = : '"  + peek + "'");
                    return null;
                }

	    case ':':
                readch(br);
                if (peek == '=') {
                    peek = ' ';
                    return Word.assign;
                } else {
                    System.err.println("Erroneous character at line " + line
                            + " after ':' : "  + peek );
                    return null;
                }
          
            case (char)-1:
                return new Token(Tag.EOF);

            default:
                if (Character.isLetter(peek) || peek == '_') {
			String id = "";

			if (peek == '_') {
				while (peek == '_') {
					id += '_';
					readch(br);
				}

				if (!Character.isLetterOrDigit(peek)) {
					System.err.println("Line " + line + ": Wront identifier format " +
						id + peek + "'.");
					return null;
				}
			}
	
			while (Character.isLetterOrDigit(peek) || peek == '_') {
				id += peek;
				readch(br);
			}
			
			if (id.equals("case"))
				return Word.casetok;
			else if (id.equals("when"))
				return Word.when;
			else if (id.equals("then"))
				return Word.then;
			else if (id.equals("else"))
				return Word.elsetok;
			else if (id.equals("while"))
				return Word.whiletok;
			else if (id.equals("do"))
				return Word.dotok;
			else if (id.equals("print"))
				return Word.print;
			else if (id.equals("read"))
				return Word.read;
			else
				return new Word(Tag.ID, id);
                } else if (Character.isDigit(peek)) {
			int acc = 0;

			while (Character.isDigit(peek)) {
				acc *= 10;
				acc += Character.getNumericValue(peek);
				readch(br);
			}

			return new NumberToken(acc);
                } else {
                        System.err.println("Erroneous character at line " + line + ": '" 
                                + peek +"'");
                        return null;
                }
	}
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = args[0];
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Token tok;
            do {
                tok = lex.lexical_scan(br);
                System.out.println("Scan: " + tok);
            } while (tok.tag != Tag.EOF);
            br.close();
        } catch (IOException e) {e.printStackTrace();}    
    }

}
