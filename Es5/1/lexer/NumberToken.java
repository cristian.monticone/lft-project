package lexer;

public class NumberToken extends Token {
    public int number = 0;
    public NumberToken(int number) { super(Tag.NUM); this.number=number; }
    public String toString() { return "<" + tag + ", " + number + ">"; }
}
