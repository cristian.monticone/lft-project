package translator;

import lexer.*;

import java.io.*;
import java.util.*;

public class Translator {
    private Lexer lex;
    private BufferedReader pbr;
    private Token look;

    private SymbolTable st = new SymbolTable();
    private CodeGenerator code = new CodeGenerator();
    private int count = 0;

    private static boolean verbose = false;

    public Translator(Lexer l, BufferedReader br) {
        lex = l;
        pbr = br;
        move();
    }

    void move() {
        look = lex.lexical_scan(pbr);
        if (verbose) System.out.println("token = " + look);
    }

    void error(String s) {
	throw new Error("near line " + lex.line + ": " + s);
    }

    void match(int t) {
    	String caller = Thread.currentThread().getStackTrace()[2].getMethodName();

	if (look.tag == t) {
	    if (look.tag != Tag.EOF) move();
	} else error("Error in grammar (" + caller + "), syntax error: " +
	             "Expected → '" + t +"', found → " + look +"'.");
    }

    private boolean testGui(int... tags) {
	for (int tag : tags)
		if (look.tag == tag) return true;

	return false;
    }

    private int getAddr(String s) {
	    int id_addr = st.lookupAddress(s);
	    if (id_addr == -1) {
		    id_addr = count;
		    st.insert(s, count++);
	    }

	    return id_addr;
    }

    public void prog() {
    	if (testGui(Tag.ID, Tag.PRINT, Tag.READ, Tag.CASE, Tag.WHILE, '{')) {
		int lnext_prog = code.newLabel();
		statlist(lnext_prog);
		code.emitLabel(lnext_prog);

		match(Tag.EOF);

		try {
			code.toJasmin();
		}
		catch(java.io.IOException e) {
			System.out.println("IO error\n");
		};
	}
	else {
		error("Error in grammar (prog) reading " + look + ": A " +
		      "program must start with a statement!");
	}
    }

    private void statlist(int sl_next) {
    	if (testGui(Tag.ID, Tag.PRINT, Tag.READ, Tag.CASE, Tag.WHILE, '{')) {
		int s_next = code.newLabel();
		stat(s_next);
		code.emitLabel(s_next);
		statlistp(sl_next);
	}
	else {
		error("Error in grammar (statlist) reading " + look +
		      ": Expected a statement.");
	}
    }

    private void statlistp(int sl_next) {
    	if (testGui(';')) {
		match(';');
		int s_next = code.newLabel();
		stat(s_next);
		code.emitLabel(s_next);
		statlistp(sl_next);
	}
	else if (testGui(Tag.EOF, '}')) {
		// Do nothing! all right :-)
	}
	else {
		error("Error in grammar (statlistp) reading " + look + 
		      ": Unexpected symbol after statement list.");
	}
    }

    private void stat(int lnext) {
    	if (testGui(Tag.ID)) {
		int id_addr = getAddr(((Word)look).lexeme);
		match(Tag.ID);
		match(Tag.ASSIGN);
		expr();
		code.emit(OpCode.istore, id_addr);
	}
	else if (testGui(Tag.PRINT)) {
		match(Tag.PRINT);
		match('(');
		expr();
		code.emit(OpCode.invokestatic, 1);
		match(')');
	}
	else if (testGui(Tag.READ)) {
		match(Tag.READ);
		match('(');
		if (look.tag == Tag.ID) {
			int read_id_addr = getAddr(((Word)look).lexeme);
			match(Tag.ID);
			match(')');
			code.emit(OpCode.invokestatic, 0);
			code.emit(OpCode.istore, read_id_addr);
		}
		else {
			error("Error in grammar (stat) after read( with " + look);
		}
	}
	else if (testGui(Tag.CASE)) {
		match(Tag.CASE);
		whenlist(lnext);
		match(Tag.ELSE);
		stat(lnext);
	}
	else if (testGui(Tag.WHILE)) {
		match(Tag.WHILE);
		match('(');
		int ltrue = code.newLabel();
		int loop_head = code.newLabel();
		code.emitLabel(loop_head);
		bexpr(ltrue, lnext);
		match(')');
		code.emitLabel(ltrue);
		stat(loop_head);
		code.emit(OpCode.GOto, loop_head);
	}
	else if (testGui('{')) {
		match('{');
		statlist(lnext);
		match('}');
	}
	else {
		error("Error in grammar (stat) reading " + look +
		      ": Expected a statement! Unexpected token.");
	}
    }

    private void whenlist(int next_whenlist) {
    	if (testGui(Tag.WHEN)) {
		int w_next = code.newLabel();
		whenitem(w_next, next_whenlist);
		code.emitLabel(w_next);
		whenlistp(next_whenlist);
	}
	else {
		error("Error in grammar (whenlist) reading " + look +
		      ": A 'case when list' must start with a WHEN.");
	}
    }

    private void whenlistp(int next_whenlist) {
    	if (testGui(Tag.WHEN)) {
		int w_next = code.newLabel();
		whenitem(w_next, next_whenlist);
		code.emitLabel(w_next);
		whenlistp(next_whenlist);
	}
	else if (testGui(Tag.ELSE)) {
		// Nothing to do here. zzZ...
	}
	else {
		error("Error in grammar (whenlistp) reading " + look +
		      ": Expected a WHEN or an ELSE! Syntax error...");
	}
    }

    private void whenitem(int next_when, int next_whenlist) {
    	if (testGui(Tag.WHEN)) {
		match(Tag.WHEN);
		match('(');
		int ltrue = code.newLabel();
		bexpr(ltrue, next_when);
		match(')');
		code.emitLabel(ltrue);
		stat(next_whenlist);
		code.emit(OpCode.GOto, next_whenlist);
	}
	else {
		error("Error in grammar (whenitem) reading " + look +
		      ": Expected a WHEN.");
	}
    }

    private void bexpr(int ltrue, int lfalse) {
    	if (testGui('(', Tag.NUM, Tag.ID)) {
		Word reloptok;
		expr();
		reloptok = (Word)look;
		match(Tag.RELOP);
		expr();
		
		String relop_s = reloptok.lexeme;
		OpCode op= null;
		if (relop_s.equals("<"))
			op = OpCode.if_icmplt;
		else if (relop_s.equals("<="))
			op = OpCode.if_icmple;
		else if (relop_s.equals(">"))
			op = OpCode.if_icmpgt;
		else if (relop_s.equals(">="))
			op = OpCode.if_icmpge;
		else if (relop_s.equals("=="))
			op = OpCode.if_icmpeq;
		else if (relop_s.equals("<>"))
			op = OpCode.if_icmpne;
		else
			error("Error in grammar(bexpr): Unexpected RELOP lexeme → '" + relop_s + "'.");

		code.emit(op, ltrue);
		code.emit(OpCode.GOto, lfalse);
	}
	else {
		error("Error in grammar (bexpr) reading " + look + 
		      ": Unexpected start in the when test! Expected the first" +
		      " expression to start with a '(', a number " +
		      "or a identifier!");
	}
    }

    private void expr() {
    	if (testGui('(', Tag.ID, Tag.NUM)) {
		term();
		exprp();
	}
	else {
		error("Error in grammar (expr) reading " + look +
		      ": Expected an expression witch start with a factor: a '(', a " +
		      "identifier or a numeric value.");
	}
    }

    private void exprp() {
	if (testGui('+')) {
		match('+');
		term();
		exprp();
		code.emit(OpCode.iadd);
	}
	else if (testGui('-')) {
		match('-');
		term();
		exprp();
		code.emit(OpCode.isub);
	}
	else if (testGui(Tag.EOF, ')', Tag.RELOP, '}', ';', Tag.WHEN, Tag.ELSE, Tag.RELOP)) {
		// nop. zzZ...
	}
	else {
		error("Error in grammar (exprp) reading " + look +
		      ": Unexpected token after one expression.");
	}
    }

    private void term() {
    	if (testGui('(', Tag.ID, Tag.NUM)) {
		fact();
		termp();
	}
	else {
		error("Error in grammar (term) reading " + look +
		      ": A term must start with a factor!");
	}
    }

    private void termp() {
    	if (testGui('*')) {
		match('*');
		fact();
		termp();
		code.emit(OpCode.imul);
	}
	else if (testGui('/')) {
		match('/');
		fact();
		termp();
		code.emit(OpCode.idiv);
	}
	else if (testGui('+', '-', '}', ';', ')', Tag.EOF, Tag.WHEN, Tag.ELSE, Tag.RELOP)) {
		// do nothing!
	}
	else {
		error("Error in grammar (termp) reading " + look +
		      "The expression must continue(+,-,/,*) or end, unexpected token." + look);
	}
    }

    private void fact() {
    	if (testGui('(')) {
		match('(');
		expr();
		match(')');
	}
	else if (testGui(Tag.ID)) {
		int id_addr = st.lookupAddress(((Word)look).lexeme);

		if (id_addr == -1)
			error("Error in grammar (fact): referencing a not declared identifier '" + look + "'.");

		code.emit(OpCode.iload, id_addr);
		match(Tag.ID);
	}
	else if (testGui(Tag.NUM)) {
		int num = ((NumberToken)look).number;
		code.emit(OpCode.ldc, num);
		match(Tag.NUM);
	}
	else {
		error("Error in grammar (fact) reading " + look +
		      ": Expected a factor!");
	}
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = args[0]; // il percorso del file da leggere

	if (args.length > 1) verbose = args[1].equals("verbose");

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Translator translator = new Translator(lex, br);
            translator.prog();
            System.out.println("\"" + args[0] + "\" compiled succesfully.");
            br.close();
        } catch (IOException e) {e.printStackTrace();}
    }
}
