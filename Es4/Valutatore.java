import java.io.*;

public class Valutatore {
    private Lexer lex;
    private BufferedReader pbr;
    private Token look;

    public Valutatore(Lexer l, BufferedReader br) {
        lex = l;
        pbr = br;
        move();
    }

    void move() {
        look = lex.lexical_scan(pbr);
    }

    void error(String s) {
	throw new Error("near line " + lex.line + ": " + s);
    }

    void match(int t) {
	if (look.tag == t) {
	    if (look.tag != Tag.EOF) move();
	} else error("syntax error");
    }

    private boolean testGui(int... tags) {
	for (int tag : tags)
		if (look.tag == tag) return true;

	return false;
    }

    public void start() {
    	double val;
    	if (testGui('(', Tag.NUM)) {
		val = expr();
		match(Tag.EOF);
		System.out.println(val);
	}
	else {
		error("An expressione must start with '(' or a numeric value.");
	}
    }

    private double expr() {
    	double term_val, exprp_val;
	double expr_val = 0;

    	if (testGui('(', Tag.NUM)) { 
		// Voglio essere verboso per leggibilità e coerenza con gli esercizi teorici.
		// Alternativa compatta: return exprp(term());
		term_val = term();
		expr_val =  exprp(term_val);
	}
	else {
		error("Expected an expression witch start with '(' or with a numeric value.");
	}

	return expr_val;
    }

    private double exprp(double exprp_i) {
	double term_val;
	double exprp_val = 0;

	if (testGui('+')) {
		match('+');
		term_val = term();
		exprp_val = exprp(exprp_i + term_val);
	}
	else if (testGui('-')) {
		match('-');
		term_val = term();
		exprp_val = exprp(exprp_i - term_val);
	}
	else if (testGui(')', Tag.EOF)) {
		exprp_val = exprp_i;
	}
	else {
		error("An expression must finish with ')' or with the end of file.");
	}

	return exprp_val;
    }

    private double term() {
    	double fact_val;
	double term_val = 0;

    	if (testGui('(', Tag.NUM)) {
		fact_val = fact();
		term_val = termp(fact_val);
	}
	else {
		error("A term must start with a factor!");
	}

	return term_val;
    }

    private double termp(double termp_i) {
    	double fact_val;
	double termp_val = 0;

    	if (testGui('*')) {
		match('*');
		fact_val = fact();
		termp_val = termp(termp_i * fact_val);
	}
	else if (testGui('/')) {
		match('/');
		fact_val = fact();
		termp_val = termp(termp_i / fact_val);
	}
	else if (testGui(Tag.EOF, '+', '-', ')')) {
		termp_val = termp_i;
	}
	else {
		error("The expression must continue(+,-,/,*) or end(')',EOF), unexpected token.");
	}

	return termp_val;
    }

    private double fact() {
    	double expr_val;
	double fact_val = 0;

    	if (testGui('(')) {
		match('(');
		expr_val = expr();
		match(')');
		fact_val = expr_val;
	}
	else if (testGui(Tag.NUM)) {
		fact_val = ((NumberToken)look).number;
		match(Tag.NUM);
	}
	else {
		error("Expected a factor!");
	}

	return fact_val;
    }
		
    public static void main(String[] args) {
        Lexer lex = new Lexer();
        String path = args[0]; // il percorso del file da leggere
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            Parser parser = new Parser(lex, br);
	    System.out.print("Expression result: ");
            parser.start();
            br.close();
        } catch (IOException e) {e.printStackTrace();}
    }
}
